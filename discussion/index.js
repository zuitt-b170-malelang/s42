document.querySelector("#txt-first-name");
/*
document - refers to the whole webpage
querySelector - used to select a specific element(object) as long as it is inside the html tag (HTML element)
						- takes a string input that is formatted like CSS selector
						-can select elements regardless if the string is an id, a class or a tag; as long as the element is existing in the webpage

*/

/*
document.querySelector is similar to these three:

document.getElementById("txt-first-name");
document.getElementByClass("txt-inputs");
document.getElementByTagName("input");
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML = txtFirstName.value
})

/*
	event - actions that the user is doing in our webpage ( scroll, click, hover, keypress/type )

	addEventListener - a function that lets the webpage to listen to the events performed by the user
								- takes two arguments
										- string - the event to which the HTML element will listen; these are predetermined
										- function - executed by the element once the event (first argument) is triggered

*/
// makes the element listen to multiple events
txtFirstName.addEventListener("keyup", (event) => {
	// trying to log the codes, instead of its value, once the event is triggered
	console.log(event.target);
	// trying to log the value of the element, once the event is triggered
	console.log(event.target.value);
})

/*
Make another "keyup" event where in the span element will record the Last Name in the forms

send the output in the batch google chat

*/

const txtLastName = document.querySelector("#txt-last-name");

txtLastName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML = txtLastName.value;

	console.log(event.target);
	console.log(event.target.value);
})

// txtLastName.addEventListener("type", (event) => {
//   txtFullName.innerHTML = txt.txtFirstName.value + " " + txtLastName.value;

//   console.log(event.target);
// 	console.log(event.target.value);
// })
